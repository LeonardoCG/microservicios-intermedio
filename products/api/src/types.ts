export type statusCode = | "success" | "error" | "notFound" | "notPermited" | "validationError";
//enpoint para habilitar
export const endpoint = [
  "create",
  "update",
  "delete",
  "findOne",
  "view",
] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
    host: string;
    port: number;
    //password: string;
};

export interface Model {
  id: number;
  name: string;
  mark: string;
  year: string;
  image: string;
  bodega: string;
  state: boolean;
  lote: number;

  createdAt: string;
  updatedAt: string;
}

export interface Paginate {
  data: Model[];
  itemCount: number;
  pageCount: number;
}

//namespace: pueden tener muchas particiones con tipos de datos
export namespace FindOne {
  export interface Request {
    id: string;
  }

  export interface Responce {
    statusCode: statusCode;
    data?: Model;
    message?: string;
  }
}

export namespace View {
  export interface Request {
    offset?: number;
    limit?: number;
    //poner en plural
    year?: number;
    state?: boolean;

    marks?: string[];
    bodegas?: string[];
    lotes?: number[];
  }

  export interface Responce {
    statusCode: statusCode;
    data?: Paginate;
    message?: string;
  }
}

export namespace Update {
  export interface Request {
    id: number;

    name?: string;
    mark?: string;
    year?: string;
    image?: string;
    bodega?: string;
    state?: boolean;
    lote?: number;
  }

  export interface Responce {
    statusCode: statusCode;
    data?: Model;
    message?: string;
  }
}

export namespace Create {
  export interface Request {
    name: string;
    mark: string;

    year?: string;
    image?: string;
    bodega?: string;
    lote?: number;
  }
  export interface Responce {
    statusCode: statusCode;
    data?: Model;
    message?: string;
  }
}

export namespace Delete {
  export interface Request {
    ids?: number[];
    marks?: string[];
    years?: string[];

    bodegas?: string[];
    lotes?: number[];

    state?: boolean;
  }

  export interface Responce {
    statusCode: statusCode;
    data?: Model;
    message?: string;
  }
}
