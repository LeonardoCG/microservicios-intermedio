import { JobsOptions } from "bullmq";
import * as T from './types';
export { T };
export declare const name = "products";
export declare const version: number;
export declare const Create: (props: T.Create.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Create.Responce>;
export declare const Delete: (props: T.Delete.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Delete.Responce>;
export declare const View: (props: T.View.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.View.Responce>;
export declare const Update: (props: T.Update.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Update.Responce>;
export declare const FindOne: (props: T.FindOne.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.FindOne.Responce>;
//# sourceMappingURL=index.d.ts.map