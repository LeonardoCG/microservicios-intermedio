export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare const endpoint: readonly ["create", "update", "delete", "findOne", "view"];
export declare type Endpoint = typeof endpoint[number];
export interface REDIS {
    host: string;
    port: number;
}
export interface Model {
    id: number;
    name: string;
    mark: string;
    year: string;
    image: string;
    bodega: string;
    state: boolean;
    lote: number;
    createdAt: string;
    updatedAt: string;
}
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export declare namespace FindOne {
    interface Request {
        id: string;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        year?: number;
        state?: boolean;
        marks?: string[];
        bodegas?: string[];
        lotes?: number[];
    }
    interface Responce {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        bodegas?: string[];
        lotes?: number[];
        state?: boolean;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map