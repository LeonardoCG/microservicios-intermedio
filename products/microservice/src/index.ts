import { SyncDB } from "./models/models";

import { run } from "./adapter/adapter";

//momento de desconexion de red
import { redisClient } from "./settings";

redisClient.on("error", (err) => console.log('Redis Client Error', err));

export { SyncDB, run };


