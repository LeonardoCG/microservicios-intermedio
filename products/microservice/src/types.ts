import * as S from "sequelize";

export type statusCode = | "success" | "error" | "notFound" | "notPermited" | "validationError";

// definimos la DB
export namespace Models {
  export interface ModelAttributes {
    id?: number;
    name?: string;
    mark?: string;
    year?: string;
    image?: string;
    code?: string;
    bodega?: string;
    state?: boolean;
    lote?: number;

    createdAt?: string;
    updatedAt?: string;

  }

  export const attributes = [ "id", "name", "mark", "year", "image","code", "bodega", "state", "lote", "createdAt","updatedAt" ] as const;

  export type Attributes = typeof attributes[number];

  export type where = S.WhereOptions<ModelAttributes>;

  export interface Model extends S.Model<ModelAttributes> {};

  export interface Paginate {
    data: ModelAttributes[];
    itemCount: number;
    pageCount: number;
  };

  export namespace SyncDB {
    export interface Request extends S.SyncOptions {};

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    };
  };

  export namespace Count {
    export interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {};
    export interface Responce {
      statusCode: statusCode;
      data?: number;
      message?: string;
    };
  };

  export namespace Create {
    export interface Request extends ModelAttributes{};

    export interface Opts extends S.CreateOptions<ModelAttributes>{};

    export interface Responce {
      statusCode: statusCode;
      data?: ModelAttributes;
      message?: string;
    };
  };

  export namespace Delete {
    export interface Opts extends S.DestroyOptions<ModelAttributes> {};

    export interface Responce {
      statusCode: statusCode;
      data?: number;
      message?: string;
    };
  };

  export namespace ViewfindAndCountAll {
    export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {};

    export interface Responce {
      statusCode: statusCode;
      data?: Paginate;
      message?: string;
    };
  };

  export namespace FindOne {
    export interface Opts extends S.FindOptions<ModelAttributes> {};

    export interface Response {
      statusCode: statusCode;
      data?: ModelAttributes;
      message?: string;
    };
  };

  export namespace Update {
    export interface Request extends ModelAttributes {}

    export interface Opts extends S.UpdateOptions<ModelAttributes> {}

    export interface Responce {
      statusCode: statusCode;
      data?: [number, ModelAttributes[]];
      message?: string;
    }
  };
};

//namespace: pueden tener muchas particiones con tipos de datos
export namespace Services {

  export namespace FindOne {
    export interface Request {
      id: string;

    };

    export interface Responce {
      statusCode: statusCode;
      data?: Models.ModelAttributes;
      message?: string;
    };
  };

  export namespace View {
    export interface Request {
      offset?: number;
      limit?: number;
      //poner en plural
      year?: number;
      state?: boolean;

      marks?: string[];
      bodegas?: string[];
      lotes?: number[];
    };

    export interface Responce {
      statusCode: statusCode;
      data?: Models.Paginate;
      message?: string;
    };
  };
  
  export namespace Update {
      export interface Request {
        id: number;

        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
      };

      export interface Responce {
        statusCode: statusCode;
        data?: Models.ModelAttributes;
        message?: string;
      };
  };

  export namespace Create {
    export interface Request {

      name: string;
      mark: string;

      year?: string;
      image?: string;
      bodega?: string;
      lote?: number;
    };
    export interface Responce {
      statusCode: statusCode;
      data?: Models.ModelAttributes;
      message?: string;
    };
  };

  export namespace Delete {
    export interface Request {
      ids?: number[];
      marks?: string[];
      years?: string[];

      bodegas?: string[];
      lotes?: number[];

      state?: boolean;
    };

    export interface Responce {
      statusCode: statusCode;
      data?: Models.ModelAttributes;
      message?: string;
    };
  };
};

export namespace Controller {
  export namespace Publish {
    
    export interface Request {
      channel: string;
      instance: string;
    };

    export interface Response {
      statusCode: statusCode;
      data?: Request;
      message?: string;
    };
  };
};

export namespace Adapters {
   //enpoint para habilitar
   export const endpoint = [ "create", "update", "delete", "findOne", "view" ] as const;

  export type Endpoint = typeof endpoint[number];

  export namespace BullConn {

      export interface opts {
        concurrency: number;
        redis: Settings.REDIS;
      };
  };
};

export namespace Settings {
 
  export interface REDIS {
    host: string;
    port: number;
    password: string;
  };
};
