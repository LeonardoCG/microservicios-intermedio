import dotenv from "dotenv";
import { Sequelize } from "sequelize";
import { Settings as T, Adapters } from "./types";
import { createClient } from "redis";

dotenv.config();

export const name: string = "products";
export const version: number = 2;

const REDIS: T.REDIS = {
  host: process.env.REDIS_HOST,
  port: parseInt(process.env.REDIS_PORT),
  password: process.env.REDIS_PASS,
};

//cliente de sequelize
export const sequelize: Sequelize = new Sequelize({
  database: process.env.POSTGRES_DB,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  host: process.env.POSTGRES_HOST,
  port: parseInt(process.env.POSTGRES_PORT),
  logging: false,
  dialect: "postgres",
});

//cliente de redis
export const redisClient: ReturnType<typeof createClient> = createClient({
  url: `redis://${REDIS.host}:${REDIS.port}`,
  password: REDIS.password,
});

//objeto que crea workers o subrutinas ligeras
export const RedisOptsQueue: Adapters.BullConn.opts = {
  concurrency: parseInt(process.env.BULL_CONCURRENCY) || 50,
  redis: {
    host: REDIS.host,
    port: REDIS.port,
    password: REDIS.password,
  },
};

export const Actions = {
  create: `${name}:create:${version}`,
  update: `${name}:update:${version}`,
  delete: `${name}:delete:${version}`,
  orphan: `${name}:orphan:${version}`, //detecta elementos que ya no existen y lo elimina
  error: `${name}:error:${version}`,
  start: `${name}: start:${version}`,
};

export const InternalError: string =
  "No podemos procesar tu solicitud en este momento";
