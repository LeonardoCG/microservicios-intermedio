import * as Models from "../models/models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from 'product-validate';

//funciones asinc await
export const createService = async ( params: T.Services.Create.Request ): Promise<T.Services.Create.Responce> => {
  try {

    await Validation.create(params);

    const { statusCode, data, message } = await Models.create(params);

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service create", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const deleteService = async ( params: T.Services.Delete.Request ): Promise<T.Services.Delete.Responce> => {
  try {

    await Validation.deleti(params);

    var where: T.Models.where = {};

    var optionals: T.Models.Attributes[] = ['id', 'mark', 'year', 'bodega', 'lote'];

    if( params.state !== undefined) where.state = params.state;
    //recorre concatena con 's' valida y corta la 's'
    for(let x of optionals.map(v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x]

    const { statusCode, data, message } = await Models.deleti({ where });

    if (statusCode !== "success") return { statusCode, message };

    return { statusCode: "success", data: data[1][0]};

  } catch (error) {

    console.error({ step: "Service delete", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const updateService = async ( params: T.Services.Update.Request ): Promise<T.Services.Update.Responce> => {
  try {

    await Validation.update(params);

    var where: T.Models.where = { id: params.id };

    const findOne = await Models.findOne({ where });

    if (findOne.statusCode !== "success") {
      switch (findOne.statusCode) {
        case "notFound": return { statusCode: "validationError", message: "producto no esta registrado" };
        default: return { statusCode: "error", message: InternalError };
      };
    };

    const { statusCode, data, message } = await Models.update(params, { where });

    if (statusCode !== "success") return { statusCode, message };

    return { statusCode: "success", data: data[1][0] };

  } catch (error) {

    console.error({ step: "Service update", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const viewService = async ( params: T.Services.View.Request ): Promise<T.Services.View.Responce> => {
  try {
    
    await Validation.view(params);
    //filtrado
    var where: T.Models.where = {};

    var optionals: T.Models.Attributes[] = ['year', 'state'];
    
    for (let x of optionals) { if (params[x] !== undefined) { where[x] = params[x] }};


    var optionals: T.Models.Attributes[] = ['mark', 'bodega', 'lote'];
    //recorre concatena con 's' valida y corta la 's'
    for(let x of optionals.map(v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

    // actualizacion
    const { statusCode, data, message } = await Models.viewfindAndCountAll({ where });

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service view", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  }
};

export const findOneService = async ( params: T.Services.FindOne.Request ): Promise<T.Services.FindOne.Responce> => {
  try {

    await Validation.findOne(params);
    //filtrado
    var where: T.Models.where = { id: params.id };

    const { statusCode, data, message } = await Models.findOne({ where });

    return { statusCode, data, message };
    

  } catch (error) {

    console.error({ step: "Service findOne", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  }
};

