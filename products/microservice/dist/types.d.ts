import * as S from "sequelize";
export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare namespace Models {
    interface ModelAttributes {
        id?: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        code?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
        createdAt?: string;
        updatedAt?: string;
    }
    const attributes: readonly ["id", "name", "mark", "year", "image", "code", "bodega", "state", "lote", "createdAt", "updatedAt"];
    type Attributes = typeof attributes[number];
    type where = S.WhereOptions<ModelAttributes>;
    interface Model extends S.Model<ModelAttributes> {
    }
    interface Paginate {
        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    }
    namespace SyncDB {
        interface Request extends S.SyncOptions {
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Count {
        interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace Create {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.CreateOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Opts extends S.DestroyOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace ViewfindAndCountAll {
        interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Opts extends S.FindOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Update {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.UpdateOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        }
    }
}
export declare namespace Services {
    namespace FindOne {
        interface Request {
            id: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            year?: number;
            state?: boolean;
            marks?: string[];
            bodegas?: string[];
            lotes?: number[];
        }
        interface Responce {
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            bodega?: string;
            state?: boolean;
            lote?: number;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            bodegas?: string[];
            lotes?: number[];
            state?: boolean;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
}
export declare namespace Controller {
    namespace Publish {
        interface Request {
            channel: string;
            instance: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Request;
            message?: string;
        }
    }
}
export declare namespace Adapters {
    const endpoint: readonly ["create", "update", "delete", "findOne", "view"];
    type Endpoint = typeof endpoint[number];
    namespace BullConn {
        interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}
export declare namespace Settings {
    interface REDIS {
        host: string;
        port: number;
        password: string;
    }
}
//# sourceMappingURL=types.d.ts.map