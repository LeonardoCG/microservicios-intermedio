"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Settings = exports.Adapters = exports.Controller = exports.Services = exports.Models = void 0;
// definimos la DB
var Models;
(function (Models) {
    Models.attributes = ["id", "name", "mark", "year", "image", "code", "bodega", "state", "lote", "createdAt", "updatedAt"];
    ;
    ;
    let SyncDB;
    (function (SyncDB) {
        ;
        ;
    })(SyncDB = Models.SyncDB || (Models.SyncDB = {}));
    ;
    let Count;
    (function (Count) {
        ;
        ;
    })(Count = Models.Count || (Models.Count = {}));
    ;
    let Create;
    (function (Create) {
        ;
        ;
        ;
    })(Create = Models.Create || (Models.Create = {}));
    ;
    let Delete;
    (function (Delete) {
        ;
        ;
    })(Delete = Models.Delete || (Models.Delete = {}));
    ;
    let ViewfindAndCountAll;
    (function (ViewfindAndCountAll) {
        ;
        ;
    })(ViewfindAndCountAll = Models.ViewfindAndCountAll || (Models.ViewfindAndCountAll = {}));
    ;
    let FindOne;
    (function (FindOne) {
        ;
        ;
    })(FindOne = Models.FindOne || (Models.FindOne = {}));
    ;
    ;
})(Models = exports.Models || (exports.Models = {}));
;
//namespace: pueden tener muchas particiones con tipos de datos
var Services;
(function (Services) {
    let FindOne;
    (function (FindOne) {
        ;
        ;
    })(FindOne = Services.FindOne || (Services.FindOne = {}));
    ;
    let View;
    (function (View) {
        ;
        ;
    })(View = Services.View || (Services.View = {}));
    ;
    let Update;
    (function (Update) {
        ;
        ;
    })(Update = Services.Update || (Services.Update = {}));
    ;
    let Create;
    (function (Create) {
        ;
        ;
    })(Create = Services.Create || (Services.Create = {}));
    ;
    let Delete;
    (function (Delete) {
        ;
        ;
    })(Delete = Services.Delete || (Services.Delete = {}));
    ;
})(Services = exports.Services || (exports.Services = {}));
;
var Controller;
(function (Controller) {
    let Publish;
    (function (Publish) {
        ;
        ;
    })(Publish = Controller.Publish || (Controller.Publish = {}));
    ;
})(Controller = exports.Controller || (exports.Controller = {}));
;
var Adapters;
(function (Adapters) {
    //enpoint para habilitar
    Adapters.endpoint = ["create", "update", "delete", "findOne", "view"];
    let BullConn;
    (function (BullConn) {
        ;
    })(BullConn = Adapters.BullConn || (Adapters.BullConn = {}));
    ;
})(Adapters = exports.Adapters || (exports.Adapters = {}));
;
var Settings;
(function (Settings) {
    ;
})(Settings = exports.Settings || (exports.Settings = {}));
;
//# sourceMappingURL=types.js.map