"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SyncDB = exports.update = exports.findOne = exports.viewfindAndCountAll = exports.deleti = exports.create = exports.count = exports.Model = void 0;
//sequelize
const S = __importStar(require("sequelize"));
const settings_1 = require("../settings");
exports.Model = settings_1.sequelize.define(settings_1.name, {
    name: { type: S.DataTypes.STRING },
    mark: { type: S.DataTypes.STRING },
    year: { type: S.DataTypes.STRING },
    image: { type: S.DataTypes.STRING },
    code: { type: S.DataTypes.STRING },
    bodega: { type: S.DataTypes.STRING },
    state: { type: S.DataTypes.BOOLEAN, defaultValue: true },
    lote: { type: S.DataTypes.BIGINT },
}, { freezeTableName: true });
//definimos todas las funciones del modelo db, create, update etc..
//FUNCIONES
const count = async (options) => {
    try {
        const instance = await exports.Model.count(options);
        return { statusCode: "success", data: instance };
    }
    catch (error) {
        console.log({ step: "Models count", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
};
exports.count = count;
const create = async (values, options) => {
    try {
        const instance = await exports.Model.create(values, options);
        return { statusCode: "success", data: instance.toJSON() };
    }
    catch (error) {
        console.log({ step: "Models create", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
};
exports.create = create;
const deleti = async (options) => {
    try {
        const instance = await exports.Model.destroy(options);
        return { statusCode: "success", data: instance };
    }
    catch (error) {
        console.log({ step: "Models delete", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
};
exports.deleti = deleti;
const viewfindAndCountAll = async (options) => {
    try {
        // nos devuelve el limite de elementos en 12 e inicia en la pagina 0
        // adicional se agrega algo mas que venga en la vista
        var options = {
            ...{ limit: 12, offset: 0 },
            ...options,
        };
        const { count, rows } = await exports.Model.findAndCountAll(options);
        return {
            statusCode: "success",
            data: {
                // la data mapeada en json
                data: rows.map((v) => v.toJSON()),
                //numero de elementos encontrado en la db
                itemCount: count,
                // division de elementos con el limite seteado
                pageCount: Math.ceil(count / options.limit),
            },
        };
    }
    catch (error) {
        console.log({ step: "Models view", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
};
exports.viewfindAndCountAll = viewfindAndCountAll;
const findOne = async (options) => {
    try {
        const instance = await exports.Model.findOne(options);
        if (instance)
            return { statusCode: "success", data: instance.toJSON() };
        else
            return { statusCode: "notFound", message: "not found" };
    }
    catch (error) {
        console.log({ step: "Models findOne", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
    ;
};
exports.findOne = findOne;
const update = async (values, options) => {
    try {
        var options = { ...{ returning: true }, ...options };
        const instances = await exports.Model.update(values, options);
        return { statusCode: "success", data: [instances[0], instances[1].map(v => v.toJSON())] };
    }
    catch (error) {
        console.log({ step: "Models update", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
    ;
};
exports.update = update;
const SyncDB = async (params) => {
    try {
        const model = await exports.Model.sync(params);
        return { statusCode: "success", data: model };
    }
    catch (error) {
        console.error({ step: "Models SyncDB", error: error.toString() });
        return { statusCode: "error", message: error.toString() };
    }
};
exports.SyncDB = SyncDB;
//# sourceMappingURL=models.js.map