import * as T from "../types";
export declare const createService: (params: T.Services.Create.Request) => Promise<T.Services.Create.Responce>;
export declare const deleteService: (params: T.Services.Delete.Request) => Promise<T.Services.Delete.Responce>;
export declare const updateService: (params: T.Services.Update.Request) => Promise<T.Services.Update.Responce>;
export declare const viewService: (params: T.Services.View.Request) => Promise<T.Services.View.Responce>;
export declare const findOneService: (params: T.Services.FindOne.Request) => Promise<T.Services.FindOne.Responce>;
//# sourceMappingURL=services.d.ts.map