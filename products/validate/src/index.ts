import Joi from "joi";
import * as T from "./types";

//funciones asinc await
export const create = async (params: T.Create.Request): Promise<T.Create.Responce> => {
  try {
    const schema = Joi.object({
      name: Joi.string().required(),
      mark: Joi.string().required(),

      year: Joi.date(),
      image: Joi.string().uri(),
      bodega: Joi.string(),
      lote: Joi.number(),
    });

    const result = await schema.validateAsync(params);

    return { statusCode: "success", data: params };

  } catch (error) {
    throw { statusCode: "error", message: error.toString() };
  }
};

export const deleti = async (params: T.Delete.Request): Promise<T.Delete.Responce> => {
  try {
    const schema = Joi.object({

      ids: Joi.array().items(Joi.number().required()),
      marks: Joi.array().items(Joi.string().required()),
      years: Joi.array().items(Joi.date().required()),
      bodegas: Joi.array().items(Joi.string().required()),
      lotes: Joi.array().items(Joi.number().required()),

      state: Joi.boolean(),

    }).or('ids','marks','years','bodegas','lotes','state'); //enviar cualquiera de estas opts

    const result = await schema.validateAsync(params);

    return { statusCode: "success", data: params };
  } catch (error) {
    throw { statusCode: "error", message: error.toString() };
  }
};

export const update = async (params: T.Update.Request): Promise<T.Update.Responce> => {
  try {
    const schema = Joi.object({

      id: Joi.number().required(),

      name: Joi.string(),
      mark: Joi.string(),
      year: Joi.date(),
      image: Joi.string().uri(),
      bodega: Joi.string(),
      lote: Joi.number(),

      state: Joi.boolean(),
    });

    const result = await schema.validateAsync(params);

    return { statusCode: "success", data: params };
  } catch (error) {
    throw { statusCode: "error", message: error.toString() };
  }
};

export const view = async (params: T.View.Request): Promise<T.View.Responce> => {
  try {
    const schema = Joi.object({
      offset: Joi.number(),
      limit: Joi.number(),

      year: Joi.string(),
      state: Joi.boolean(),

      marks: Joi.array().items(Joi.string().required()),
      bodegas: Joi.array().items(Joi.string().required()),
      lotes: Joi.array().items(Joi.number().required()),
    });

    const result = await schema.validateAsync(params);

    return { statusCode: "success", data: params };
  } catch (error) {
    throw { statusCode: "error", message: error.toString() };
  }
};

export const findOne = async (params: T.FindOne.Request): Promise<T.FindOne.Responce> => {
  try {
    const schema = Joi.object({
      id: Joi.number().required(),
    });

    const result = await schema.validateAsync(params);

    return { statusCode: "success", data: params };
  } catch (error) {
    throw { statusCode: "error", message: error.toString() };
  }
};
