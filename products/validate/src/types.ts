
export type statusCode =
  | "success"
  | "error"
  | "notFound"
  | "notPermited"
  | "validationError";

//namespace: pueden tener muchas particiones con tipos de datos
export namespace FindOne {
  export interface Request {
    id: string;

  };

  export interface Responce {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};

export namespace View {
  export interface Request {
    offset?: number;
    limit?: number;
    //poner en plural
    year?: number;
    state?: boolean;

    marks?: string[];
    bodegas?: string[];
    lotes?: number[];
  };

  export interface Responce {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};

export namespace Update {
    export interface Request {
      id: number;

      name?: string;
      mark?: string;
      year?: string;
      image?: string;
      bodega?: string;
      state?: boolean;
      lote?: number;
    };

    export interface Responce {
      statusCode: statusCode;
      data?: Request;
      message?: string;
    };
};

export namespace Create {
  export interface Request {

    name: string;
    mark: string;

    year?: string;
    image?: string;
    bodega?: string;
    lote?: number;
  };
  export interface Responce {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};

export namespace Delete {
  export interface Request {
    ids?: number[];
    marks?: string[];
    years?: string[];

    bodegas?: string[];
    lotes?: number[];

    state?: boolean;
  };

  export interface Responce {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};