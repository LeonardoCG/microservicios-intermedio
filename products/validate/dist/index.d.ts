import * as T from "./types";
export declare const create: (params: T.Create.Request) => Promise<T.Create.Responce>;
export declare const deleti: (params: T.Delete.Request) => Promise<T.Delete.Responce>;
export declare const update: (params: T.Update.Request) => Promise<T.Update.Responce>;
export declare const view: (params: T.View.Request) => Promise<T.View.Responce>;
export declare const findOne: (params: T.FindOne.Request) => Promise<T.FindOne.Responce>;
//# sourceMappingURL=index.d.ts.map