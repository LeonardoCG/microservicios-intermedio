export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare namespace FindOne {
    interface Request {
        id: string;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        year?: number;
        state?: boolean;
        marks?: string[];
        bodegas?: string[];
        lotes?: number[];
    }
    interface Responce {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        state?: boolean;
        lote?: number;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        name: string;
        mark: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        bodegas?: string[];
        lotes?: number[];
        state?: boolean;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map