export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: string[];
        products?: string[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        user: string;
        product: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: string[];
        prodcuts?: string[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map