"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.view = exports.deleti = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
//funciones asinc await
const create = async (params) => {
    try {
        const schema = joi_1.default.object({
            user: joi_1.default.number(),
            product: joi_1.default.number(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.create = create;
const deleti = async (params) => {
    try {
        const schema = joi_1.default.object({
            ids: joi_1.default.array().items(joi_1.default.number().required()),
            users: joi_1.default.array().items(joi_1.default.string().required()),
            products: joi_1.default.array().items(joi_1.default.string().required()),
        }).or('ids', 'user', 'product'); //enviar cualquiera de estas opts
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.deleti = deleti;
const view = async (params) => {
    try {
        const schema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            users: joi_1.default.array().items(joi_1.default.string().required()),
            products: joi_1.default.array().items(joi_1.default.string().required()),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.view = view;
//# sourceMappingURL=index.js.map