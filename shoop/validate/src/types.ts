
export type statusCode = | "success" | "error" | "notFound" | "notPermited" | "validationError";

//namespace: pueden tener muchas particiones con tipos de datos
export namespace View {
  export interface Request {
      offset?: number;
      limit?: number;
      //poner en plural
      users?: string[];
      products?: string[];
  };

  export interface Response {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};

export namespace Create {
  export interface Request {
    user: string;
    product: string;
  };
  export interface Response {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};

export namespace Delete {
  export interface Request {
      ids?: number[];

      users?: string[];
      prodcuts?: string[];
    };

  export interface Response {
    statusCode: statusCode;
    data?: Request;
    message?: string;
  };
};
