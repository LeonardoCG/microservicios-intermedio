"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.run = exports.Process = void 0;
const settings_1 = require("../settings");
const Services = __importStar(require("../services/services"));
const bullmq_1 = require("bullmq");
const controllers_1 = require("../controller/controllers");
const Process = async (job) => {
    try {
        switch (job.name) {
            case 'create':
                {
                    //retorna lo que tiene creado el servicio
                    let { statusCode, data, message } = await Services.createService(job.data);
                    return { statusCode, data, message };
                }
                ;
            case 'delete':
                {
                    let { statusCode, data, message } = await Services.deleteService(job.data);
                    return { statusCode, data, message };
                }
                ;
            case 'view':
                {
                    let { statusCode, data, message } = await Services.viewService(job.data);
                    return { statusCode, data, message };
                }
                ;
            default: return { statusCode: "error", message: "Method not found" };
        }
        ;
    }
    catch (error) {
        await (0, controllers_1.Publish)({
            channel: settings_1.Actions.start,
            instance: JSON.stringify({ step: 'Adapters Run ', message: `tarting ${settings_1.name}` })
        });
    }
    ;
};
exports.Process = Process;
const run = async () => {
    try {
        await (0, controllers_1.Publish)({
            channel: settings_1.Actions.start,
            instance: JSON.stringify({ step: 'Adapters Run ', message: `starting ${settings_1.name}` })
        });
        //fundamental la version 2
        const worker = new bullmq_1.Worker(`${settings_1.name}:2`, exports.Process, { connection: settings_1.RedisOptsQueue.redis, concurrency: settings_1.RedisOptsQueue.concurrency });
        worker.on('error', async (error) => {
            await (0, controllers_1.Publish)({
                channel: settings_1.Actions.error,
                instance: JSON.stringify({ step: 'Adapters Run ', error })
            });
        });
    }
    catch (error) {
        await (0, controllers_1.Publish)({
            channel: settings_1.Actions.error,
            instance: JSON.stringify({ step: 'Adapters Run ', error })
        });
    }
    ;
};
exports.run = run;
//# sourceMappingURL=adapter.js.map