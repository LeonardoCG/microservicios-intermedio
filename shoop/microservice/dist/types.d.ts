import * as S from "sequelize";
import { T as apiUser } from 'api-user';
import { T as apiProduct } from 'api-product';
export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare namespace Models {
    interface ModelAttributes {
        id?: number;
        user?: string;
        product?: string;
        createdAt?: string;
        updatedAt?: string;
    }
    const attributes: readonly ["id", "user", "product", "createdAt", "updatedAt"];
    type Attributes = typeof attributes[number];
    type where = S.WhereOptions<ModelAttributes>;
    interface Model extends S.Model<ModelAttributes> {
    }
    interface Paginate {
        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    }
    namespace SyncDB {
        interface Request extends S.SyncOptions {
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Count {
        interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace Create {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.CreateOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Opts extends S.DestroyOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace ViewfindAndCountAll {
        interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Opts extends S.FindOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Update {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.UpdateOptions<ModelAttributes> {
        }
        interface Responce {
            statusCode: statusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        }
    }
}
export declare namespace Services {
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            user: string;
            product: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: string[];
            prodcuts?: string[];
        }
        interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
}
export declare namespace Controller {
    namespace ValidateUser {
        interface Request {
            user: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: apiUser.Model;
            message: string;
        }
    }
    namespace ValidateProduct {
        interface Request {
            product: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: apiProduct.Model;
            message: string;
        }
    }
    namespace Publish {
        interface Request {
            channel: string;
            instance: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Request;
            message?: string;
        }
    }
}
export declare namespace Adapters {
    const endpoint: readonly ["create", "delete", "view"];
    type Endpoint = typeof endpoint[number];
    namespace BullConn {
        interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}
export declare namespace Settings {
    interface REDIS {
        host: string;
        port: number;
        password: string;
    }
}
//# sourceMappingURL=types.d.ts.map