"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.viewService = exports.deleteService = exports.createService = void 0;
const Models = __importStar(require("../models/models"));
const settings_1 = require("../settings");
const Validation = __importStar(require("shoop-validate"));
const Controller = __importStar(require("../controller/controllers"));
//funciones asinc await
const createService = async (params) => {
    try {
        await Validation.create(params);
        //validacion del users
        const user = (await Controller.ValidateUser({ user: params.user })).data;
        if (!user.state) {
            throw { statusCode: 'validationError', message: "El usuario no esta habilidado para realizar compras" };
        }
        //validacion de products
        const product = (await Controller.ValidateProduct({ product: params.product })).data;
        if (!user.state) {
            throw { statusCode: 'validationError', message: "El producto no hay stock para realizar ventas" };
        }
        const { statusCode, data, message } = await Models.create(params);
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service create", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
    ;
};
exports.createService = createService;
const deleteService = async (params) => {
    try {
        await Validation.deleti(params);
        var where = {};
        var optionals = ['id', 'user', 'product'];
        //recorre concatena con 's' valida y corta la 's'
        for (let x of optionals.map(v => v.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        const { statusCode, data, message } = await Models.deleti({ where });
        if (statusCode !== "success")
            return { statusCode, message };
        return { statusCode: "success", data: data[1][0] };
    }
    catch (error) {
        console.error({ step: "Service delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
    ;
};
exports.deleteService = deleteService;
const viewService = async (params) => {
    try {
        await Validation.view(params);
        //filtrado
        var where = {};
        var optionals = ['user', 'product'];
        //recorre concatena con 's' valida y corta la 's'
        for (let x of optionals.map(v => v.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        // actualizacion
        const { statusCode, data, message } = await Models.viewfindAndCountAll({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service view", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.viewService = viewService;
//# sourceMappingURL=services.js.map