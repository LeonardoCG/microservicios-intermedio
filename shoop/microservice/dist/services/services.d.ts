import * as T from "../types";
export declare const createService: (params: T.Services.Create.Request) => Promise<T.Services.Create.Response>;
export declare const deleteService: (params: T.Services.Delete.Request) => Promise<T.Services.Delete.Response>;
export declare const viewService: (params: T.Services.View.Request) => Promise<T.Services.View.Response>;
//# sourceMappingURL=services.d.ts.map