"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InternalError = exports.Actions = exports.RedisOptsQueue = exports.redisClient = exports.sequelize = exports.version = exports.name = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const sequelize_1 = require("sequelize");
const redis_1 = require("redis");
dotenv_1.default.config();
exports.name = "products";
exports.version = 2;
const REDIS = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASS,
};
//cliente de sequelize
exports.sequelize = new sequelize_1.Sequelize({
    database: process.env.POSTGRES_DB,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.POSTGRES_HOST,
    port: parseInt(process.env.POSTGRES_PORT),
    logging: false,
    dialect: "postgres",
});
//cliente de redis
exports.redisClient = (0, redis_1.createClient)({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password,
});
//objeto que crea workers o subrutinas ligeras
exports.RedisOptsQueue = {
    concurrency: parseInt(process.env.BULL_CONCURRENCY) || 50,
    redis: {
        host: REDIS.host,
        port: REDIS.port,
        password: REDIS.password,
    },
};
exports.Actions = {
    create: `${exports.name}:create:${exports.version}`,
    update: `${exports.name}:update:${exports.version}`,
    delete: `${exports.name}:delete:${exports.version}`,
    orphan: `${exports.name}:orphan:${exports.version}`,
    error: `${exports.name}:error:${exports.version}`,
    start: `${exports.name}: start:${exports.version}`,
};
exports.InternalError = "No podemos procesar tu solicitud en este momento";
//# sourceMappingURL=settings.js.map