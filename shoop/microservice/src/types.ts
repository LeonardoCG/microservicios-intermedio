import * as S from "sequelize";
import{ T as apiUser } from 'api-user';
import{ T as apiProduct } from 'api-product';

export type statusCode = | "success" | "error" | "notFound" | "notPermited" | "validationError";

// definimos la DB
export namespace Models {
  export interface ModelAttributes {
    id?: number;
    user?: string;
    product?: string;

    createdAt?: string;
    updatedAt?: string;

  }

  export const attributes = [ "id", "user", "product", "createdAt","updatedAt" ] as const;

  export type Attributes = typeof attributes[number];

  export type where = S.WhereOptions<ModelAttributes>;

  export interface Model extends S.Model<ModelAttributes> {};

  export interface Paginate {
    data: ModelAttributes[];
    itemCount: number;
    pageCount: number;
  };

  export namespace SyncDB {
    export interface Request extends S.SyncOptions {};

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    };
  };

  export namespace Count {
    export interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {};
    export interface Responce {
      statusCode: statusCode;
      data?: number;
      message?: string;
    };
  };

  export namespace Create {
    export interface Request extends ModelAttributes{};

    export interface Opts extends S.CreateOptions<ModelAttributes>{};

    export interface Responce {
      statusCode: statusCode;
      data?: ModelAttributes;
      message?: string;
    };
  };

  export namespace Delete {
    export interface Opts extends S.DestroyOptions<ModelAttributes> {};

    export interface Responce {
      statusCode: statusCode;
      data?: number;
      message?: string;
    };
  };

  export namespace ViewfindAndCountAll {
    export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {};

    export interface Responce {
      statusCode: statusCode;
      data?: Paginate;
      message?: string;
    };
  };

  export namespace FindOne {
    export interface Opts extends S.FindOptions<ModelAttributes> {};

    export interface Response {
      statusCode: statusCode;
      data?: ModelAttributes;
      message?: string;
    };
  };

  export namespace Update {
    export interface Request extends ModelAttributes {}

    export interface Opts extends S.UpdateOptions<ModelAttributes> {}

    export interface Responce {
      statusCode: statusCode;
      data?: [number, ModelAttributes[]];
      message?: string;
    }
  };
};

//namespace: pueden tener muchas particiones con tipos de datos
export namespace Services {

  export namespace View {
    export interface Request {
      offset?: number;
      limit?: number;
      //poner en plural
      users?: string[];
      products?: string[];
    };

    export interface Response {
      statusCode: statusCode;
      data?: Models.Paginate;
      message?: string;
    };
  };

  export namespace Create {
    export interface Request {
      user: string;
      product: string;
    };
    export interface Response {
      statusCode: statusCode;
      data?: Models.ModelAttributes;
      message?: string;
    };
  };

  export namespace Delete {
    export interface Request {
      ids?: number[];
      users?: string[];
      prodcuts?: string[];
    };

    export interface Response {
      statusCode: statusCode;
      data?: Models.ModelAttributes;
      message?: string;
    };
  };
};

export namespace Controller {

  export namespace ValidateUser {

    export interface Request {
      user: string;
    };
    export interface Response {
      statusCode: statusCode;
      data?: apiUser.Model;
      message: string;
    };
  };

  export namespace ValidateProduct {

    export interface Request {
      product: string;
    };

    export interface Response {
      statusCode: statusCode;
      data?: apiProduct.Model;
      message: string;
    };
  };
  export namespace Publish {
    
    export interface Request {
      channel: string;
      instance: string;
    };

    export interface Response {
      statusCode: statusCode;
      data?: Request;
      message?: string;
    };
  };
};

export namespace Adapters {
   //enpoint para habilitar
   export const endpoint = [ "create", "delete", "view" ] as const;

  export type Endpoint = typeof endpoint[number];

  export namespace BullConn {

      export interface opts {
        concurrency: number;
        redis: Settings.REDIS;
      };
  };
};

export namespace Settings {
 
  export interface REDIS {
    host: string;
    port: number;
    password: string;
  };
};
