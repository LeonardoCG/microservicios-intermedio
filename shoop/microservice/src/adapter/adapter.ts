import { name, RedisOptsQueue as opts, Actions } from '../settings'; 
import * as Services from '../services/services';
import { Worker, Job } from 'bullmq';
import { Publish } from '../controller/controllers';
import { Adapters } from '../types'; 

export const Process = async(job: Job<any, any, Adapters.Endpoint> ) => {
    try {

        switch(job.name) {

            case 'create': {
                //retorna lo que tiene creado el servicio
                let { statusCode, data, message } = await Services.createService(job.data);

                return { statusCode, data, message };
            };

            case 'delete': {
                let { statusCode, data, message } = await Services.deleteService(job.data);

                return { statusCode, data, message };
            };

            case 'view': {
                let { statusCode, data, message } = await Services.viewService(job.data);

                return { statusCode, data, message };
            };
            
            default: return { statusCode: "error", message: "Method not found" };
        };
        
    } catch (error) {

        await Publish({ 
        channel: Actions.start, 
        instance: JSON.stringify(
            { step: 'Adapters Run ', message: `tarting ${name}`})
        });
    };
};

export const run = async() => {
  try {

    await Publish({ 
        channel: Actions.start, 
        instance: JSON.stringify(
            { step: 'Adapters Run ', message: `starting ${name}`})
        });
        
    //fundamental la version 2
    const worker = new Worker(`${name}:2`, Process, { connection: opts.redis, concurrency: opts.concurrency });

    worker.on('error', async error => {
        await Publish({ 
            channel: Actions.error, 
            instance: JSON.stringify(
                { step: 'Adapters Run ', error})
            });
    });

  } catch (error) {
        await Publish({ 
            channel: Actions.error, 
            instance: JSON.stringify(
                { step: 'Adapters Run ', error})
            });
  };
};