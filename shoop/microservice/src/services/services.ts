import * as Models from "../models/models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from 'shoop-validate';

import * as Controller from '../controller/controllers';

//funciones asinc await
export const createService = async ( params: T.Services.Create.Request ): Promise<T.Services.Create.Response> => {
  try {

    await Validation.create(params);

    //validacion del users
    const user = (await Controller.ValidateUser({ user: params.user })).data;

    if (!user.state) {
      throw { statusCode: 'validationError', message: "El usuario no esta habilidado para realizar compras" };
    }
    
    //validacion de products
    const product = (await Controller.ValidateProduct({ product: params.product })).data;

    if (!user.state) {
      throw { statusCode: 'validationError', message: "El producto no hay stock para realizar ventas" };
    }

    const { statusCode, data, message } = await Models.create(params);

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service create", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const deleteService = async ( params: T.Services.Delete.Request ): Promise<T.Services.Delete.Response> => {
  try {

    await Validation.deleti(params);

    var where: T.Models.where = {};

    var optionals: T.Models.Attributes[] = ['id', 'user', 'product'];
    //recorre concatena con 's' valida y corta la 's'
    for(let x of optionals.map(v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x]

    const { statusCode, data, message } = await Models.deleti({ where });

    if (statusCode !== "success") return { statusCode, message };

    return { statusCode: "success", data: data[1][0]};

  } catch (error) {

    console.error({ step: "Service delete", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const viewService = async ( params: T.Services.View.Request ): Promise<T.Services.View.Response> => {
  try {
    
    await Validation.view(params);
    //filtrado
    var where: T.Models.where = {};

    var optionals: T.Models.Attributes[] = ['user', 'product'];
    //recorre concatena con 's' valida y corta la 's'
    for(let x of optionals.map(v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

    // actualizacion
    const { statusCode, data, message } = await Models.viewfindAndCountAll({ where });

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service view", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  }
};


