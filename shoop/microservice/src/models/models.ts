//sequelize
import * as S from "sequelize";
import { sequelize, name } from "../settings";
import { Models as T } from "../types";

export const Model = sequelize.define<T.Model>(name,
  {
    user: { type: S.DataTypes.STRING },
    product: { type: S.DataTypes.STRING },
  },
  { freezeTableName: true }
);

//definimos todas las funciones del modelo db, create, update etc..
//FUNCIONES

export const count = async ( options?: T.Count.Request): Promise<T.Count.Responce> => {
  try {
    const instance: number = await Model.count(options);

    return { statusCode: "success", data: instance };
  } catch (error) {
    console.log({ step: "Models count", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  }
};

export const create = async ( values: T.Create.Request, options?: T.Create.Opts ): Promise<T.Create.Responce> => {
  try {
    const instance: T.Model = await Model.create(values, options);

    return { statusCode: "success", data: instance.toJSON() };
  } catch (error) {
    console.log({ step: "Models create", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  }
};

export const deleti = async ( options?: T.Delete.Opts): Promise<T.Delete.Responce> => {
  try {
    const instance: number = await Model.destroy(options);

    return { statusCode: "success", data: instance };
  } catch (error) {
    console.log({ step: "Models delete", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  }
};

export const viewfindAndCountAll = async ( options?: T.ViewfindAndCountAll.Opts): Promise<T.ViewfindAndCountAll.Responce> => {
  try {
    // nos devuelve el limite de elementos en 12 e inicia en la pagina 0
    // adicional se agrega algo mas que venga en la vista
    var options: T.ViewfindAndCountAll.Opts = {
      ...{ limit: 12, offset: 0 },
      ...options,
    };

    const { count, rows }: { rows: T.Model[]; count: number } =
      await Model.findAndCountAll(options);

    return {
      statusCode: "success",
      data: {
        // la data mapeada en json
        data: rows.map((v) => v.toJSON()),
        //numero de elementos encontrado en la db
        itemCount: count,
        // division de elementos con el limite seteado
        pageCount: Math.ceil(count / options.limit),
      },
    };
  } catch (error) {
    console.log({ step: "Models view", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  }
};

export const findOne = async( options?: T.FindOne.Opts): Promise<T.FindOne.Response> => {
  try {
    const instance: T.Model | null = await Model.findOne(options);
    
    if (instance) return { statusCode: "success", data: instance.toJSON() };
    
    else return { statusCode: "notFound", message: "not found" };

  } catch (error) {
    console.log({ step: "Models findOne", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  };
};

export const update = async (values: T.Update.Request, options?: T.Update.Opts): Promise<T.Update.Responce> => {
  try {
    
    var options: T.Update.Opts = { ...{ returning: true  }, ...options };
    
    const instances: [ number, T.Model[] ] = await Model.update(values, options);

    return { statusCode: "success", data: [instances[0], instances[1].map(v => v.toJSON())] };

  } catch (error) {
    console.log({ step: "Models update", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  };

};

export const SyncDB = async (params: T.SyncDB.Request): Promise<T.SyncDB.Responce> => {
  try {
    const model: T.Model = await Model.sync(params);

    return { statusCode: "success", data: model };
  } catch (error) {
    console.error({ step: "Models SyncDB", error: error.toString() });

    return { statusCode: "error", message: error.toString() };
  }
};
