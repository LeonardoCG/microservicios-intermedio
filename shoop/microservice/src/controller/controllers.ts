import { InternalError, redisClient, RedisOptsQueue as opts } from '../settings';
import { Controller as T } from '../types';

import * as apiUser from 'api-user';
import * as apiProduct from 'api-product';

//funcion para conectar APIs externas
export const Publish = async(props: T.Publish.Request): Promise<T.Publish.Response> => {
  try {

    if (!redisClient.isOpen) await redisClient.connect();

    await redisClient.publish(props.channel, props.instance);

    return { statusCode: 'success', data: props };
    
  } catch (error) {
    
      console.error('error', { step: 'controller Publish', error });

      return { statusCode: 'error', message: InternalError };
  };
};

export const ValidateUser =async (props:T.ValidateUser.Request): Promise<T.ValidateUser.Response> => {
  try {
      //solicitamos autentificacion del usuario conectando a redis
      const { statusCode, data, message } = await apiUser.FindOne({id: props.user, username: ''}, opts.redis);

      if( statusCode !== 'success') {

        switch (statusCode) {

          case 'notFound': throw { statusCode: 'validationError', message: "No existe el usuario que validas" };
                  
          default: throw { statusCode, message };
        }
      };

      return { statusCode: 'success', data, message };
    
  } catch (error) {
      console.error('error', { step: 'controller validateUser', error });

      return { statusCode: 'error', message: InternalError };
  };
};

export const ValidateProduct =async (props:T.ValidateProduct.Request): Promise<T.ValidateProduct.Response> => {
  try {
    //solicitamos autentificacion del usuario conectando a redis
    const { statusCode, data, message } = await apiProduct.FindOne({id: props.product}, opts.redis);

    if( statusCode !== 'success') {

      switch (statusCode) {

        case 'notFound': throw { statusCode: 'validationError', message: "No existe el producto que validas" };
                
        default: throw { statusCode, message };
      }
    };

    return { statusCode: 'success', data, message };

  } catch (error) {
      console.error('error', { step: 'controller validateProduct', error });

      return { statusCode: 'error', message: InternalError };
  };
};