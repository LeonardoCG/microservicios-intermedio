const { SyncDB, run } = require('./dist'); 

(async () => {
    try {
        const result = await SyncDB({ force: false });
        //borrar la db y estar logueado
        //const result = await SyncDB({ force: true, logging: true});

        if (result.statusCode !== "success") throw result.message;

        await run();

    } catch (error) { console.log(error) }


})();