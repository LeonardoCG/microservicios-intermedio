export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare const endpoint: readonly ["create", "delete", "view"];
export declare type Endpoint = typeof endpoint[number];
export interface REDIS {
    host: string;
    port: number;
}
export interface Model {
    id: number;
    user: string;
    product: string;
    createdAt: string;
    updatedAt: string;
}
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: string[];
        products?: string[];
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Create {
    interface Request {
        user: string;
        product: string;
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: string[];
        prodcuts?: string[];
    }
    interface Responce {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map