"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.View = exports.Delete = exports.Create = exports.version = exports.name = exports.T = void 0;
const bullmq_1 = require("bullmq");
const T = __importStar(require("./types"));
exports.T = T;
exports.name = "products";
exports.version = 2;
const Create = async (props, redis, opts) => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = 'create';
        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await bullmq_1.Job.fromId(queue, job.id);
        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};
exports.Create = Create;
const Delete = async (props, redis, opts) => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = 'delete';
        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await bullmq_1.Job.fromId(queue, job.id);
        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};
exports.Delete = Delete;
const View = async (props, redis, opts) => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = 'view';
        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await bullmq_1.Job.fromId(queue, job.id);
        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};
exports.View = View;
//# sourceMappingURL=index.js.map