import { Queue, QueueEvents, Job, JobsOptions } from "bullmq";
import * as T from './types';

export { T };

export const name = "users";
export const version: number = 2;

export const Create = async (props: T.Create.Request, redis: T.REDIS, opts?: JobsOptions): Promise<T.Create.Responce> => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue: Queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, {connection: redis });

        const endpoint: T.Endpoint = 'create';

        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await Job.fromId(queue, job.id);

        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message};


    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};

export const Delete = async (props: T.Delete.Request, redis: T.REDIS, opts?: JobsOptions): Promise<T.Delete.Responce> => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue: Queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, {connection: redis });

        const endpoint: T.Endpoint = 'delete';

        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await Job.fromId(queue, job.id);

        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message};


    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};

export const View = async (props: T.View.Request, redis: T.REDIS, opts?: JobsOptions): Promise<T.View.Responce> => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue: Queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, {connection: redis });

        const endpoint: T.Endpoint = 'view';

        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);

        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await Job.fromId(queue, job.id);

        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message};


    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};

export const Update = async (props: T.Update.Request, redis: T.REDIS, opts?: JobsOptions): Promise<T.Update.Responce> => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue: Queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, {connection: redis });

        const endpoint: T.Endpoint = 'update';

        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await Job.fromId(queue, job.id);

        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message};


    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};

export const FindOne = async (props: T.FindOne.Request, redis: T.REDIS, opts?: JobsOptions): Promise<T.FindOne.Responce> => {
    try {
        //definicion de parametros y conexion con bullmq y eventos
        const queue: Queue = new Queue(`${name}:${version}`, { connection: redis });

        const queueEvents: QueueEvents = new QueueEvents(`${name}:${version}`, {connection: redis });

        const endpoint: T.Endpoint = 'findOne';

        // incorporar la tarea
        const job = await queue.add(endpoint, props, opts);
        //esperar la tarea
        await job.waitUntilFinished(queueEvents);
        //obtener el ID de la tarea
        const result = await Job.fromId(queue, job.id);

        //retornar la tarea
        const { statusCode, data, message } = result.returnvalue;

        return { statusCode, data, message};


    } catch (error) {
        throw { statusCode: 'error', message: error.toString() };
    }
};


