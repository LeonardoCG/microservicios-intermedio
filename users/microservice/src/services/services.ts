import * as Models from "../models/models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from 'user-validate';

//funciones asinc await
export const createService = async ( params: T.Services.Create.Request ): Promise<T.Services.Create.Response> => {
  try {

    await Validation.create(params);

    const findOne = await Models.findOne({ where: { username: params.username } });

    if (findOne.statusCode !== 'notFound') {
      switch (findOne.statusCode) {
        case 'success': return { statusCode: 'validationError', message: "usuario ya registrado" };
        
        default: return { statusCode: "error", message: InternalError };
      };
    };

    const { statusCode, data, message } = await Models.create(params);

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service create", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const deleteService = async ( params: T.Services.Delete.Request ): Promise<T.Services.Delete.Response> => {
  try {

    await Validation.deleti(params);

    var where: T.Models.where = { username: params };

    const findOne = await Models.findOne({where});

    if (findOne.statusCode !== "success") {
      switch (findOne.statusCode) {
        case "notFound": return { statusCode: "validationError", message: "usuario eliminado" };
        default: return { statusCode: "error", message: InternalError };
      };
    };

    const { statusCode, message } = await Models.deleti({ where });

    if (statusCode !== "success") return { statusCode, message };

    return { statusCode: "success", data: findOne.data };

  } catch (error) {

    console.error({ step: "Service delete", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const updateService = async ( params: T.Services.Update.Request ): Promise<T.Services.Update.Response> => {
  try {

    await Validation.update(params);

    var where: T.Models.where = { username: params.username, state: true };

    const findOne = await Models.findOne({ where });

    if (findOne.statusCode !== "success") {
      switch (findOne.statusCode) {
        case "notFound": return { statusCode: "validationError", message: "usuario no esta registrado" };
        default: return { statusCode: "error", message: InternalError };
      };
    };

    if (!findOne.data.state) {
      return { statusCode: "notPermited", message: "Tu usuario esta deshabilitado" };
    };

    const { statusCode, data, message } = await Models.update(params, { where });

    if (statusCode !== "success") return { statusCode, message };

    return { statusCode: "success", data: data[1][0] };

  } catch (error) {

    console.error({ step: "Service update", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  };
};

export const viewService = async ( params: T.Services.View.Request ): Promise<T.Services.View.Response> => {
  try {
    
    await Validation.view(params);
    //filtrado
    var where: T.Models.where = {};

    var optional: T.Models.Attributes[] = ["state"];

    for (let x of optional) {
      if (params[x] !== undefined) { where[x] = params[x] };
    };
    // actualizacion
    const { statusCode, data, message } = await Models.viewfindAndCountAll({ where });

    return { statusCode, data, message };

  } catch (error) {

    console.error({ step: "Service view", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  }
};

export const findOneService = async ( params: T.Services.FindOne.Request ): Promise<T.Services.FindOne.Response> => {
  try {

    await Validation.findOne(params);
    //filtrado
    var where: T.Models.where = {};


 var optional: T.Models.Attributes[] = ['id', 'username'];

    for (let x of optional) {
      if (params[x] !== undefined) { where[x] = params[x] };
    };
    // actualizacion
    const { statusCode, data, message } = await Models.findOne({ where });

    return { statusCode, data, message };
    

  } catch (error) {

    console.error({ step: "Service findOne", error: error.toString() });

    return { statusCode: "error", message: InternalError };
  }
};

