"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.run = exports.SyncDB = void 0;
const models_1 = require("./models/models");
Object.defineProperty(exports, "SyncDB", { enumerable: true, get: function () { return models_1.SyncDB; } });
const adapter_1 = require("./adapter/adapter");
Object.defineProperty(exports, "run", { enumerable: true, get: function () { return adapter_1.run; } });
//momento de desconexion de red
const settings_1 = require("./settings");
settings_1.redisClient.on("error", (err) => console.log('Redis Client Error', err));
//# sourceMappingURL=index.js.map