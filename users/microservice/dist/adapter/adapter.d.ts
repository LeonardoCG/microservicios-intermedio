import { Job } from 'bullmq';
import { Adapters } from '../types';
export declare const Process: (job: Job<any, any, Adapters.Endpoint>) => Promise<{
    statusCode: import("../types").statusCode;
    data: import("../types").Models.ModelAttributes;
    message: string;
} | {
    statusCode: import("../types").statusCode;
    data: import("../types").Models.Paginate;
    message: string;
} | {
    statusCode: string;
    message: string;
    data?: undefined;
}>;
export declare const run: () => Promise<void>;
//# sourceMappingURL=adapter.d.ts.map