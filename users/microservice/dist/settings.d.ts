import { Sequelize } from "sequelize";
import { Adapters } from "./types";
import { createClient } from "redis";
export declare const name: string;
export declare const version: number;
export declare const sequelize: Sequelize;
export declare const redisClient: ReturnType<typeof createClient>;
export declare const RedisOptsQueue: Adapters.BullConn.opts;
export declare const Actions: {
    create: string;
    update: string;
    delete: string;
    orphan: string;
    error: string;
    start: string;
};
export declare const InternalError: string;
//# sourceMappingURL=settings.d.ts.map