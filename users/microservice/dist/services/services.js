"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOneService = exports.viewService = exports.updateService = exports.deleteService = exports.createService = void 0;
const Models = __importStar(require("../models/models"));
const settings_1 = require("../settings");
const Validation = __importStar(require("user-validate"));
//funciones asinc await
const createService = async (params) => {
    try {
        await Validation.create(params);
        const findOne = await Models.findOne({ where: { username: params.username } });
        if (findOne.statusCode !== 'notFound') {
            switch (findOne.statusCode) {
                case 'success': return { statusCode: 'validationError', message: "usuario ya registrado" };
                default: return { statusCode: "error", message: settings_1.InternalError };
            }
            ;
        }
        ;
        const { statusCode, data, message } = await Models.create(params);
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service create", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
    ;
};
exports.createService = createService;
const deleteService = async (params) => {
    try {
        await Validation.deleti(params);
        var where = { username: params };
        const findOne = await Models.findOne({ where });
        if (findOne.statusCode !== "success") {
            switch (findOne.statusCode) {
                case "notFound": return { statusCode: "validationError", message: "usuario eliminado" };
                default: return { statusCode: "error", message: settings_1.InternalError };
            }
            ;
        }
        ;
        const { statusCode, message } = await Models.deleti({ where });
        if (statusCode !== "success")
            return { statusCode, message };
        return { statusCode: "success", data: findOne.data };
    }
    catch (error) {
        console.error({ step: "Service delete", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
    ;
};
exports.deleteService = deleteService;
const updateService = async (params) => {
    try {
        await Validation.update(params);
        var where = { username: params.username, state: true };
        const findOne = await Models.findOne({ where });
        if (findOne.statusCode !== "success") {
            switch (findOne.statusCode) {
                case "notFound": return { statusCode: "validationError", message: "usuario no esta registrado" };
                default: return { statusCode: "error", message: settings_1.InternalError };
            }
            ;
        }
        ;
        if (!findOne.data.state) {
            return { statusCode: "notPermited", message: "Tu usuario esta deshabilitado" };
        }
        ;
        const { statusCode, data, message } = await Models.update(params, { where });
        if (statusCode !== "success")
            return { statusCode, message };
        return { statusCode: "success", data: data[1][0] };
    }
    catch (error) {
        console.error({ step: "Service update", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
    ;
};
exports.updateService = updateService;
const viewService = async (params) => {
    try {
        await Validation.view(params);
        //filtrado
        var where = {};
        var optional = ["state"];
        for (let x of optional) {
            if (params[x] !== undefined) {
                where[x] = params[x];
            }
            ;
        }
        ;
        // actualizacion
        const { statusCode, data, message } = await Models.viewfindAndCountAll({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service view", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.viewService = viewService;
const findOneService = async (params) => {
    try {
        await Validation.findOne(params);
        //filtrado
        var where = {};
        var optional = ['id', 'username'];
        for (let x of optional) {
            if (params[x] !== undefined) {
                where[x] = params[x];
            }
            ;
        }
        ;
        // actualizacion
        const { statusCode, data, message } = await Models.findOne({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: "Service findOne", error: error.toString() });
        return { statusCode: "error", message: settings_1.InternalError };
    }
};
exports.findOneService = findOneService;
//# sourceMappingURL=services.js.map