import * as T from "../types";
export declare const createService: (params: T.Services.Create.Request) => Promise<T.Services.Create.Response>;
export declare const deleteService: (params: T.Services.Delete.Request) => Promise<T.Services.Delete.Response>;
export declare const updateService: (params: T.Services.Update.Request) => Promise<T.Services.Update.Response>;
export declare const viewService: (params: T.Services.View.Request) => Promise<T.Services.View.Response>;
export declare const findOneService: (params: T.Services.FindOne.Request) => Promise<T.Services.FindOne.Response>;
//# sourceMappingURL=services.d.ts.map