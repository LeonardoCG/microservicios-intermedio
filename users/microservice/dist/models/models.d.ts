import * as S from "sequelize";
import { Models as T } from "../types";
export declare const Model: S.ModelCtor<T.Model>;
export declare const count: (options?: T.Count.Request) => Promise<T.Count.Response>;
export declare const create: (values: T.Create.Request, options?: T.Create.Opts) => Promise<T.Create.Response>;
export declare const deleti: (options?: T.Delete.Opts) => Promise<T.Delete.Response>;
export declare const viewfindAndCountAll: (options?: T.ViewfindAndCountAll.Opts) => Promise<T.ViewfindAndCountAll.Response>;
export declare const findOne: (options?: T.FindOne.Opts) => Promise<T.FindOne.Response>;
export declare const update: (values: T.Update.Request, options?: T.Update.Opts) => Promise<T.Update.Response>;
export declare const SyncDB: (params: T.SyncDB.Request) => Promise<T.SyncDB.Response>;
//# sourceMappingURL=models.d.ts.map