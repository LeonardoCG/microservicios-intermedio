"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const socket_io_1 = require("socket.io");
const settings_1 = require("./settings");
const apiUser = __importStar(require("api-user"));
const apiProduct = __importStar(require("api-product"));
const apiShoop = __importStar(require("api-shoop"));
const app = (0, express_1.default)();
const server = http_1.default.createServer(app);
const io = new socket_io_1.Server(server);
server.listen(80, () => {
    console.log("sever inicialize");
    io.on('connnection', socket => {
        console.log("new connetion", socket.id);
        // endppoints
        socket.on('req:user:view', async (params) => {
            try {
                console.log('req:user:view');
                const { statusCode, data, message } = await apiUser.View(params, settings_1.redis);
                return io.to(socket.id).emit('req:user:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:user:create', async (params) => {
            try {
                console.log('req:user:create');
                const { statusCode, data, message } = await apiUser.Create(params, settings_1.redis);
                return io.to(socket.id).emit('req:user:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:user:delete', async (params) => {
            try {
                console.log('req:user:delete');
                const { statusCode, data, message } = await apiUser.Delete(params, settings_1.redis);
                return io.to(socket.id).emit('req:user:delete', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
        socket.on('req:user:update', async (params) => {
            try {
                console.log('req:user:update');
                const { statusCode, data, message } = await apiUser.Update(params, settings_1.redis);
                return io.to(socket.id).emit('req:user:update', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
        socket.on('req:user:findone', async (params) => {
            try {
                console.log('req:user:findone');
                const { statusCode, data, message } = await apiUser.FindOne(params, settings_1.redis);
                return io.to(socket.id).emit('req:user:findone', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
        //endpoint products
        socket.on('req:product:view', async (params) => {
            try {
                console.log('req:product:view');
                const { statusCode, data, message } = await apiProduct.View(params, settings_1.redis);
                return io.to(socket.id).emit('req:product:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:product:create', async (params) => {
            try {
                console.log('req:product:create');
                const { statusCode, data, message } = await apiProduct.Create(params, settings_1.redis);
                return io.to(socket.id).emit('req:product:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:product:delete', async (params) => {
            try {
                console.log('req:product:delete');
                const { statusCode, data, message } = await apiProduct.Delete(params, settings_1.redis);
                return io.to(socket.id).emit('req:product:delete', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
        socket.on('req:product:update', async (params) => {
            try {
                console.log('req:product:update');
                const { statusCode, data, message } = await apiProduct.Update(params, settings_1.redis);
                return io.to(socket.id).emit('req:product:update', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
        // endpoint shoop 
        socket.on('req:shoop:view', async (params) => {
            try {
                console.log('req:shoop:view');
                const { statusCode, data, message } = await apiShoop.View(params, settings_1.redis);
                return io.to(socket.id).emit('req:shoop:view', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:shoop:create', async (params) => {
            try {
                console.log('req:shoop:create');
                const { statusCode, data, message } = await apiShoop.Create(params, settings_1.redis);
                return io.to(socket.id).emit('req:shoop:create', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
        });
        socket.on('req:shoop:delete', async (params) => {
            try {
                console.log('req:shoop:delete');
                const { statusCode, data, message } = await apiShoop.Delete(params, settings_1.redis);
                return io.to(socket.id).emit('req:shoop:delete', { statusCode, data, message });
            }
            catch (error) {
                console.log(error);
            }
            ;
        });
    });
});
//# sourceMappingURL=index.js.map