import express, { Express } from 'express';
import http from 'http';
import { Server } from 'socket.io';
import { redis } from './settings';

import * as apiUser from 'api-user';
import * as apiProduct from 'api-product';
import * as apiShoop from 'api-shoop';

const app: Express = express();

const server: http.Server = http.createServer(app);

const io = new Server( server );

server.listen(80, () => {
    console.log("sever inicialize");

    io.on('connnection', socket => {
    
        console.log("new connetion", socket.id);

    // endppoints
        socket.on('req:user:view',async (params: apiUser.T.View.Request) => {
            try {
                console.log('req:user:view');

                const { statusCode, data, message } = await apiUser.View(params, redis);

                return io.to(socket.id).emit('req:user:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:user:create',async (params: apiUser.T.Create.Request) => {
            try {
                console.log('req:user:create');

                const { statusCode, data, message } = await apiUser.Create(params, redis);
                
                return io.to(socket.id).emit('req:user:create', { statusCode, data, message });
            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:user:delete',async (params: apiUser.T.Delete.Request) => {
            try {
                console.log('req:user:delete');

                const { statusCode, data, message } = await apiUser.Delete(params, redis);

                return io.to(socket.id).emit('req:user:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            };
        });

        socket.on('req:user:update',async (params: apiUser.T.Update.Request) => {
            try {
                console.log('req:user:update');
                
                const { statusCode, data, message } = await apiUser.Update(params, redis,);

                return io.to(socket.id).emit('req:user:update', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            };
        });

        socket.on('req:user:findone', async (params: apiUser.T.FindOne.Request) => {
            try {
                console.log('req:user:findone');

                const { statusCode, data, message } = await apiUser.FindOne(params, redis);

                return io.to(socket.id).emit('req:user:findone', { statusCode, data, message });
                
            } catch (error) {
                console.log(error);
            };
        });

        //endpoint products
        socket.on('req:product:view',async (params: apiProduct.T.View.Request) => {
            try {
                console.log('req:product:view');

                const { statusCode, data, message } = await apiProduct.View(params, redis);

                return io.to(socket.id).emit('req:product:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:product:create',async (params: apiProduct.T.Create.Request) => {
            try {
                console.log('req:product:create');

                const { statusCode, data, message } = await apiProduct.Create(params, redis);
                
                return io.to(socket.id).emit('req:product:create', { statusCode, data, message });
            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:product:delete',async (params: apiProduct.T.Delete.Request) => {
            try {
                console.log('req:product:delete');

                const { statusCode, data, message } = await apiProduct.Delete(params, redis);

                return io.to(socket.id).emit('req:product:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            };
        });

        socket.on('req:product:update',async (params: apiProduct.T.Update.Request) => {
            try {
                console.log('req:product:update');
                
                const { statusCode, data, message } = await apiProduct.Update(params, redis);

                return io.to(socket.id).emit('req:product:update', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            };
        });
    
        // endpoint shoop 
        socket.on('req:shoop:view',async (params: apiShoop.T.View.Request) => {
            try {
                console.log('req:shoop:view');

                const { statusCode, data, message } = await apiShoop.View(params, redis);

                return io.to(socket.id).emit('req:shoop:view', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:shoop:create',async (params: apiShoop.T.Create.Request) => {
            try {
                console.log('req:shoop:create');

                const { statusCode, data, message } = await apiShoop.Create(params, redis);
                
                return io.to(socket.id).emit('req:shoop:create', { statusCode, data, message });
            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:shoop:delete',async (params: apiShoop.T.Delete.Request) => {
            try {
                console.log('req:shoop:delete');

                const { statusCode, data, message } = await apiShoop.Delete(params, redis);

                return io.to(socket.id).emit('req:shoop:delete', { statusCode, data, message });
            } catch (error) {
                console.log(error);
            };
        });    
    });
});