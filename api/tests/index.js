const api = require('../dist');

const redis = {
    local: {
        host: "192.168.1.106",
        port: 6379,
        //password: undefined
    }
}

const enviroment = redis.local;

const users = [
    {
        fullName: "Leonardo Cach",
        image:"https://nodejs.org/static/images/logo.svg",
        phone: "9992643991",
        username: "lcach2022" 
    },
    {
        fullName: "Andres Cach",
        image:"https://nodejs.org/static/images/logo.svg",
        phone: "9992643991",
        username: "lcach2022" 
    }
]

const create = async(user) => {
    try {

        const result = await api.Create(user, enviroment);

        console.log(result);
        
    } catch (error) {
        console.error(error);
    }
}

const deleti = async(username) => {
    try {

        const result = await api.Delete({username}, enviroment);

        console.log(result);
        
        
    } catch (error) {console.error(error)}
}

const update = async(params) => {
    try {

        const result = await api.Update(params, enviroment);

        console.log(result);
        
        
    } catch (error) {console.error(error)}
}

const findOne = async(params) => {
    try {
        
        const result = await api.FindOne(params, enviroment);

        console.log(result);
        
    } catch (error) {console.error(error)}
}

const view = async(params) => {
    try {
        
        const result = await api.View(params, enviroment);

        console.log(result);
        
    } catch (error) {console.error(error)}
}

const main = async() => {
    try {

        await view();

        //await create(users[2]);
        await deleti(users[0].username);
        await view();

       
        
    } catch (error) {
        console.error(error)
    }

}

main()