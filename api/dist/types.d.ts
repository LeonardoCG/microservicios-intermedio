export declare type statusCode = "success" | "error" | "notFound" | "notPermited" | "validationError";
export declare namespace Users {
    interface Model {
        id: number;
        username: string;
        password: string;
        fullName: string;
        image: string;
        state: boolean;
        phone: number;
        createdAt: string;
        updatedAt: string;
    }
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    namespace FindOne {
        interface Request {
            username: string;
            id: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            state?: boolean;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            username: string;
            fullName: string;
            phone?: number;
            image?: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            username: string;
            fullName: string;
            phone: number;
            image: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            username: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
}
export declare namespace Products {
    interface Model {
        id: number;
        name: string;
        mark: string;
        year: string;
        image: string;
        bodega: string;
        state: boolean;
        lote: number;
        createdAt: string;
        updatedAt: string;
    }
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    namespace FindOne {
        interface Request {
            id: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            year?: number;
            state?: boolean;
            marks?: string[];
            bodegas?: string[];
            lotes?: number[];
        }
        interface Responce {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            bodega?: string;
            state?: boolean;
            lote?: number;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            name: string;
            mark: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            bodegas?: string[];
            lotes?: number[];
            state?: boolean;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
}
export declare namespace Shoop {
    interface Model {
        id: number;
        user: string;
        product: string;
        createdAt: string;
        updatedAt: string;
    }
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            user: string;
            product: string;
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: string[];
            prodcuts?: string[];
        }
        interface Responce {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
}
//# sourceMappingURL=types.d.ts.map