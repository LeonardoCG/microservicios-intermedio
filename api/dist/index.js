"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_client_1 = require("socket.io-client");
const socket = (0, socket_io_client_1.io)("http://localhost");
async function main() {
    try {
        setTimeout(() => console.log(socket.id), 1500);
        socket.on('connect error', (err) => {
            console.log(err.message);
        });
        socket.on("disconnect", (msg) => {
            console.log("client disconnected from server");
        });
        //Users
        socket.on('res:user:view', ({ statusCode, data, message }) => {
            console.log('res:user:view', { statusCode, data, message });
        });
        socket.on('res:user:create', ({ statusCode, data, message }) => {
            console.log('res:user:create', { statusCode, data, message });
        });
        socket.on('res:user:delete', ({ statusCode, data, message }) => {
            console.log('res:user:delete', { statusCode, data, message });
        });
        socket.on('res:user:update', ({ statusCode, data, message }) => {
            console.log('res:user:update', { statusCode, data, message });
        });
        socket.on('res:user:findOne', ({ statusCode, data, message }) => {
            console.log('res:user:findOne', { statusCode, data, message });
        });
        //Product
        socket.on('res:product:view', ({ statusCode, data, message }) => {
            console.log('res:product:view', { statusCode, data, message });
        });
        socket.on('res:product:create', ({ statusCode, data, message }) => {
            console.log('res:product:create', { statusCode, data, message });
        });
        socket.on('res:product:delete', ({ statusCode, data, message }) => {
            console.log('res:product:delete', { statusCode, data, message });
        });
        //shoops 
        socket.on('res:shoop:view', ({ statusCode, data, message }) => {
            console.log('res:shoop:view', { statusCode, data, message });
        });
        socket.on('res:shoop:create', ({ statusCode, data, message }) => {
            console.log('res:shoop:create', { statusCode, data, message });
        });
        socket.on('res:shoop:delete', ({ statusCode, data, message }) => {
            console.log('res:shoop:delete', { statusCode, data, message });
        });
        //Emits
        // setTimeout(() => socket.emit('req:user:create', ({ name:'Jose', user:'jorago', email:'jorago75@gmail.com', age: 41, nationality: 'Venezolana', image:'' })));
        // setTimeout(() => socket.emit('req:user:delete', ({ id: 1 })));
        // setTimeout(() => socket.emit('req:user:update', ({ name: 'Jose Gomez', id: 1})));
        // setTimeout(() => socket.emit('req:user:findOne', ({ id: 1 })));
        setTimeout(() => {
            socket.emit('req:user:view', ({}));
            1;
        }, 300);
        //emit product 
        // setTimeout(() => socket.emit('req:product:create', ({ user:1, shoop:'1, monto: 200000, fecha:'2022-07-17' })));
        // setTimeout(() => socket.emit('req:product:delete', ({ id: 1 })));
        // setTimeout(() => socket.emit('req:product:view', ({})));
        //emit shoop 
        // setTimeout(() => socket.emit('req:shoop:create', ({ name: 'Mercy Corps', nacionalidad:'Estados Unidos)', imagen:'' })));
        // setTimeout(() => socket.emit('req:shoop:delete', ({ id: 1 })));
        //setTimeout(() => socket.emit('req:shoop:view', ({})));
    }
    catch (error) {
        console.log(error);
    }
}
main();
//# sourceMappingURL=index.js.map