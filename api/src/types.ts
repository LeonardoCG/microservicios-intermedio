export type statusCode =
  | "success"
  | "error"
  | "notFound"
  | "notPermited"
  | "validationError";

export namespace Users {
  export interface Model {
    id: number;
    username: string;
    password: string;
    fullName: string;
    image: string;
    state: boolean;
    phone: number;

    createdAt: string;
    updatedAt: string;
  }

  export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
  }

  //namespace: pueden tener muchas particiones con tipos de datos
  export namespace FindOne {
    export interface Request {
      username: string;
      id: string;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace View {
    export interface Request {
      offset?: number;
      limit?: number;
      state?: boolean;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Paginate;
      message?: string;
    }
  }

  export namespace Update {
    export interface Request {
      username: string;
      fullName: string;
      phone?: number;
      image?: string;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Create {
    export interface Request {
      username: string;
      fullName: string;
      phone: number;
      image: string;
    }
    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Delete {
    export interface Request {
      username: string;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }
};

export namespace Products {
  export interface Model {
    id: number;
    name: string;
    mark: string;
    year: string;
    image: string;
    bodega: string;
    state: boolean;
    lote: number;

    createdAt: string;
    updatedAt: string;
  }

  export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
  }

  //namespace: pueden tener muchas particiones con tipos de datos
  export namespace FindOne {
    export interface Request {
      id: string;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace View {
    export interface Request {
      offset?: number;
      limit?: number;
      //poner en plural
      year?: number;
      state?: boolean;

      marks?: string[];
      bodegas?: string[];
      lotes?: number[];
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Paginate;
      message?: string;
    }
  }

  export namespace Update {
    export interface Request {
      id: number;

      name?: string;
      mark?: string;
      year?: string;
      image?: string;
      bodega?: string;
      state?: boolean;
      lote?: number;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Create {
    export interface Request {
      name: string;
      mark: string;

      year?: string;
      image?: string;
      bodega?: string;
      lote?: number;
    }
    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Delete {
    export interface Request {
      ids?: number[];
      marks?: string[];
      years?: string[];

      bodegas?: string[];
      lotes?: number[];

      state?: boolean;
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }
};

export namespace Shoop {
  export interface Model {
    id: number;
    
    user: string;
    product: string;

    createdAt: string;
    updatedAt: string;
  }

  export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
  }

  //namespace: pueden tener muchas particiones con tipos de datos
  export namespace View {
    export interface Request {
      offset?: number;
      limit?: number;
      //poner en plural
      users?: string[];
      products?: string[];
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Create {
    export interface Request {
      user: string;
      product: string;
    }
    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }

  export namespace Delete {
    export interface Request {
      ids?: number[];
      users?: string[];
      prodcuts?: string[];
    }

    export interface Responce {
      statusCode: statusCode;
      data?: Model;
      message?: string;
    }
  }
};
