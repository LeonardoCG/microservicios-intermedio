import * as user from 'users';
import * as product from 'products';
import * as shoop from 'shoop';

(async () => {
    try {

        await user.SyncDB({ force: true });
        await product.SyncDB({ force: true });
        await shoop.SyncDB({ force: true });

        user.run();
        product.run();
        shoop.run();

    } catch (error) {
        console.log(error)
    }
})();